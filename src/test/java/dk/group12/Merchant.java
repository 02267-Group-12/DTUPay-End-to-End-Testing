package dk.group12;

import io.quarkus.test.junit.QuarkusTest;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import dk.group12.msg.AccountReq;

import java.util.UUID;

import static io.restassured.RestAssured.given;

class Payment{
  UUID token; 
  int amount; 
  String description;
  public Payment(){}
  public Payment(UUID token, int amount, String description){
    this.token = token;
    this.amount = amount;
    this.description = description;
  }
}

@QuarkusTest
public class Merchant extends DTU{

  static String bankUser(String first, String last, String cpr, int balance){
    return "";
  }

  static UUID register(String name, String bank, int HTTPCODE){
    return DTU.register(name, bank, "merchant", HTTPCODE);
  }

  static String[][] report(UUID id, int HTTPCODE){
    return DTU.report(id, "merchant", HTTPCODE);
  }
  

  static void pay(UUID id, UUID token, int amount, String description, int HTTPCODE){
    given()
      .header("Content-Type", MIME)
      .body(new Payment(token, amount, description))
      .accept(MIME)
      .when().post("/merchant/" + id + "/pay")
      .then().statusCode(HTTPCODE);
  }

  @Test
  public void testMerchantRegistrationAndVerify() {
    Merchant.register("Marie", "Spar Nord", HTTP.OK);
  }

  @Test
  public void testMerchantRegistrationWithExsistingUser() {
    Merchant.register("Erik", "Spar Nord", HTTP.OK);
    Merchant.register("Erik", "Spar Nord", HTTP.CONFLICT);
  }

  @Test
  public void testReportMerchant() {
    final var valgeir = Merchant.register("Valgeir", "Spar Nord", HTTP.OK);
    Merchant.report(valgeir, HTTP.OK);
  }

  @Test
  public void testReportMerchantWithNonExsistingUser() {
    Merchant.report(UUID.randomUUID(), HTTP.NOT_FOUND);
  }

  @Test
  public void testPay() {
    final var marie = Merchant.register(
      "Marie", 
      bankUser(
        "Marie", 
        "x", 
        "150102-7654", 
        100
      ), 
      HTTP.OK);
    final var jonas = Customer.register(
      "Jonas", 
      bankUser(
        "Jonas", 
        "x", 
        "150102-7654", 
        100
      ), 
      HTTP.OK
    );

    Customer.addToken(jonas, 1, HTTP.OK);
    UUID[] tokens = Customer.getTokens(jonas, 1, HTTP.OK);

    Merchant.pay(marie, tokens[0], 1, "test", HTTP.OK);
  }
}