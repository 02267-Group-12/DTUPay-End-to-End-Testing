package dk.group12;

public class HTTP {
    public static final int
        OK = 200,
        CREATED = 201,
        ACCEPTED = 202,
        BAD_REQUEST = 400,
        NOT_FOUND = 404,
        CONFLICT = 409

    ;
}
