package dk.group12;

import io.quarkus.test.junit.QuarkusTest;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import dk.group12.msg.AccountReq;

import java.util.UUID;

import static io.restassured.RestAssured.given;



@QuarkusTest
public class Customer extends DTU {
  static UUID register(String name, String bank, int HTTPCODE){
    return DTU.register(name, bank, "customer", HTTPCODE);
  }

  static String[][] report(UUID id, int HTTPCODE){
    return DTU.report(id, "customer", HTTPCODE);
  }

  static void addToken(UUID id, int amount, int HTTPCODE){
    given()
      .header("Content-Type", MIME)
      .body(amount)
      .accept(MIME)
      .when().post("/customer/" + id + "/tokens")
      .then().statusCode(HTTPCODE);
  }

  static UUID[] getTokens(UUID id, int amount, int HTTPCODE){
    final var tokens =  given()
      .when().get("/customer/" + id + "/tokens")
      .then().statusCode(HTTPCODE)
      .extract().as(UUID[].class);

    Assert.assertEquals(amount, tokens.length);
    return tokens;
  }

  @Test
  public void testCustomerRegistrationAndVerify() {
    Customer.register("Marie", "Spar Nord", HTTP.OK);
  }

  @Test
  public void testCustomerRegistrationWithExsistingUser() {
    Customer.register("Erik", "Spar Nord", HTTP.OK);
    Customer.register("Erik", "Spar Nord", HTTP.CONFLICT);
  }

  @Test
  public void testReportCustomer() {
    final var erik = Customer.register("Valgeir", "Spar Nord", HTTP.OK);
    Customer.report(erik, HTTP.OK);
  }
  
  @Test
  public void testReportCustomerWithNonExistingCustomer() {
    Customer.report(UUID.randomUUID(), HTTP.NOT_FOUND);
  }
 
  @Test
  public void testAddToken() {
    final var valgeir = Customer.register("Valgeir", "Spar Nord", HTTP.OK);
    Customer.addToken(valgeir, 2, HTTP.OK);
  }

  @Test
  public void testAddTokenTwice() {
    final var valgeir = Customer.register("Valgeir", "Spar Nord", HTTP.OK);
    Customer.addToken(valgeir, 1, HTTP.OK);
    Customer.addToken(valgeir, 2, HTTP.OK);
  }

  @Test
  public void testGetTokens() {
    final var jonas = Customer.register("Jonas", "Spar Nord", HTTP.OK);
    Customer.addToken(jonas, 2, HTTP.OK);
    Customer.getTokens(jonas, 2, HTTP.OK);
  }

  @Test
  public void testAddTokenWithNonExistingCustomer() {
    Customer.addToken(UUID.randomUUID(), 2, HTTP.NOT_FOUND);
  }

  @Test
  public void testAddTokenWithNegativeAmount() {
    final var chris = Customer.register("Chris", "Spar Nord", HTTP.OK);
    Customer.addToken(chris, -2, HTTP.BAD_REQUEST);
  }

  @Test
  public void testGetTokensWithNonExistingCustomer() {
    Customer
      .getTokens(UUID.randomUUID(), 1, HTTP.NOT_FOUND);
  }
}