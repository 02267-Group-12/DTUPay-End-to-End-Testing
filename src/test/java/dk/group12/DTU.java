package dk.group12;

import org.junit.Assert;

import dk.group12.msg.AccountReq;

import java.util.UUID;

import static io.restassured.RestAssured.given;

class Account{
    public String accountId = null;
    public String bankAccountId = null;
    public Account(){}
    public Account(String accountId, String bankAccountId){
        this.accountId = accountId;
        this.bankAccountId = bankAccountId;
    }
}

public class DTU {
    public static final String MIME = "application/json";

    static UUID register(String name, String bank, String type, int HTTPCODE) {
        return given()
            .header("Content-Type", MIME)
            .body(new Account(name, bank))
            .accept(MIME)
            .when().post("/" + type)
            .then().statusCode(HTTPCODE)
            .extract().body().as(UUID.class);
    }

    static String[][] report(UUID id, String type, int HTTPCODE) {
        return given()
            .when().get("/" + type + "/" + id)
            .then().statusCode(HTTPCODE)
            .extract().body().as(String[][].class);
    }
}
